# Install flutter avec Android Studio sous Linux

Ouvrir un terminal et désarchivez le SDK et AndroidStudio :

- `tar xvzf /opt/Android.tgz`
- `tar xvzf /opt/android-studio-XXXX.w.y.z-linux.tar.gz`

Lancement initial de AndroidStudio pour tester :

- `cd android-studio/bin/`
- `./studio.sh`

## Install Flutter sous Linux

documentée ici :
 https://docs.flutter.dev/get-started/install/linux


On télécharge la branche stable de flutter :

git clone https://github.com/flutter/flutter.git -b stable

dans le .bashrc ajoutez :

```bash
export PATH=$PATH:/home/oX457XX/flutter/bin
export CHROME_EXECUTABLE=/usr/bin/chromium-browser
```

On essaie :

`flutter doctor`

L'install du SDK Dart est complétée et si tout va bien :

```
p4457@info21-01:~$ flutter doctor
Doctor summary (to see all details, run flutter doctor -v):
[✓] Flutter (Channel stable, 2.5.3, on Ubuntu 20.04.3 LTS 5.4.0-90-generic,
    locale fr_FR.UTF-8)
[✓] Android toolchain - develop for Android devices (Android SDK version 31.0.0)
[✓] Chrome - develop for the web
[✓] Linux toolchain - develop for Linux desktop
[✓] Android Studio (version 2020.3)
[✓] VS Code (version 1.62.3)
[✓] Connected device (2 available)

• No issues found!
```

Pour activer le support Linux Desktop :

`flutter config --enable-linux-desktop`

On relance androidstudio et on y installe les plugins Dart et Flutter

On crée un projet en décochant la cible iOS et en sélectionnant toutes les autres.

## Install Flutter Windows

Ajoutez le PATH de Flutter à la variable d'environnement PATH
Puis :

```bash
flutter channel beta
flutter upgrade
flutter config --enable-web
```

Dans le panneau de configuration, ajouter une variable d'environnement utilisateur nommée CHROME_EXECUTABLE pointant sur l'emplacement de Chrome, du style "C:\Program Files\Google\Chrome\Application\chrome.exe"

Pour le support Desktop sous Windows, il vous faudra Visual Studio 2022 pour Flutter 2.9 beta au moins.
Attention: Visual Studio est différent de Visual Studio Code.
Pour Win32, vous avez besoin de l'outil “Desktop development with , incluant tous les composants par défaut.
Pour UWP vous aurez besoin de l'outil “Universal Windows Platform development” , avec les outils correspondants UWP C++.

## Ajout du support Desktop ou Web a posteriori sur un projet

Si on n'a pas créé le projet pour certains supports (web, desktop, etc.) à l'avance, faire, en se plaçant dans le dossier du projet :

`flutter create .`

 Pour ajouter un support spécifique faire simplement :

`flutter create  --platforms=linux,macos,web .`

## Pour faire  le build 

`flutter build web`

puis `flutter run`

## Pour lancer une app avec une platerforme spécifique  

(choisir votre plateforme)

`flutter run -d macos`



