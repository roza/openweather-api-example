import 'dart:convert';
import 'package:http/http.dart';
import 'package:openweather_app/model/weather-model.dart';
import 'package:openweather_app/util/forecast_util.dart';

class Network{
  Future<WeatherModel> getWeather({String cityName}) async{
    var url = "https://api.openweathermap.org/data/2.5/forecast/daily?q=" +
        cityName + "&units=metric&lang=fr&appid=" + Util.appId;

    final response = await get(Uri.encodeFull(url));
    print("URL: ${Uri.encodeFull(url)}");

    if (response.statusCode == 200){
      print("Weather data: ${response.body}");
      return WeatherModel.fromJson(json.decode(response.body));
    }else{
      throw Exception("Error getting json");
    }
  }
}