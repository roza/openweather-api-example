

import 'package:flutter/material.dart';
import 'package:openweather_app/model/weather-model.dart';
import 'package:openweather_app/ui/bottom_view.dart';
import 'package:openweather_app/ui/mid_view.dart';

import 'network/network.dart';

class WeatherWidget extends StatefulWidget{
  @override
  _WeatherState createState() => _WeatherState();
}

class _WeatherState extends State<WeatherWidget>{
  Future<WeatherModel> weather;
  String _cityName="Poitiers";

  @override
  void initState(){
    super.initState();
    print("initState");
    weather = getWeatherFromNetwork(cityName: _cityName);
    // weather.then((w) {
    //   print(w.city.name);
    // });
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      // appBar: AppBar(
      //   title: Text("Weather"),
      // ),
      body: ListView(
        children: [
          textFieldView(),
          Container(
            child:FutureBuilder<WeatherModel>(
              future: weather,
              builder: (BuildContext context, AsyncSnapshot<WeatherModel> snapshot){
                if (snapshot.hasData){
                  return Column(
                    children: [
                      midView(snapshot),
                      bottomView(snapshot, context)
                    ],
                  );
                }else{
                  return Container(
                    child: Center(child: CircularProgressIndicator(),),
                  );
                }
              },
            )
          )
        ],
      ),
    );
  }

  Widget textFieldView() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: TextField(
                decoration: InputDecoration(
                    hintText: "Enter City Name",
                    prefixIcon: Icon(Icons.search),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                  contentPadding: EdgeInsets.all(10)
                ),
              onSubmitted:(value){
                  setState(() {
                    _cityName = value;
                    weather =getWeatherFromNetwork(cityName: _cityName);
                  });
              },
              ),
      ),
    );
  }

  Future<WeatherModel> getWeatherFromNetwork({String cityName}) => new Network().getWeather(cityName: _cityName);
}