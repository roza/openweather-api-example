import 'package:flutter/material.dart';
import 'package:openweather_app/model/weather-model.dart';

import 'forecast_card.dart';

Widget bottomView(AsyncSnapshot<WeatherModel> snapshot,BuildContext context){
  var forecastList = snapshot.data.list;

  return Column(
    mainAxisAlignment: MainAxisAlignment.start,
    children: [
      Text("7-Day Weather Forecast".toUpperCase(),
      style: TextStyle(
        fontSize: 13,
        color: Colors.black87
      ),),
      Container(
        height:170,
        padding: EdgeInsets.symmetric(vertical: 16,horizontal: 10),
        child: ListView.separated(
          scrollDirection: Axis.horizontal,
          separatorBuilder: (context,index) => SizedBox(width: 8,),
          itemCount: forecastList.length,
          itemBuilder: (context,index) => ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            child: Container(
              width: MediaQuery.of(context).size.width / 2,
              height: 160,
              child: forecastCard(snapshot,index),
              decoration: BoxDecoration(
                gradient: LinearGradient(colors: [Colors.grey.shade400,Colors.white],
                begin: Alignment.topLeft,end: Alignment.bottomRight)
              ),
            ),
          ),
        ),
      )
    ],
  );
}

