import "package:flutter/material.dart";
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:openweather_app/model/weather-model.dart';
import 'package:openweather_app/util/convert-icon.dart';
import 'package:openweather_app/util/forecast_util.dart';


Widget forecastCard(AsyncSnapshot<WeatherModel> snapshot, int index) {
  var forecasList =snapshot.data.list;
  var dayOfWeek="";
  var fullDate = Util.getFormattedDate(
    new DateTime.fromMillisecondsSinceEpoch(forecasList[index].dt * 1000)
  );
  
  dayOfWeek = fullDate.split(" ")[0];

  return Column(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Center(child:Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(dayOfWeek),
      )),
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          CircleAvatar(
            radius: 33,
            backgroundColor: Colors.white,
            child: getOpenWeatherIcon(description: forecasList[index].weather[0].icon,color: Colors.pinkAccent,size: 80)
            //getWeatherIcon(description: forecasList[index].weather[0].main,color: Colors.pinkAccent,size: 45),
          ),
          Column(
            children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left:8.0,right: 8),
                    child: Text("${forecasList[index].temp.min.toStringAsFixed(0)} ºC"),
                  ),
                  Icon(FontAwesomeIcons.solidArrowAltCircleDown, color: Colors.white,size: 17,)
                ],
              ),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left:8.0,right: 8),
                    child: Text("${forecasList[index].temp.max.toStringAsFixed(0)} ºC"),
                  ),
                  Icon(FontAwesomeIcons.solidArrowAltCircleUp, color: Colors.white,size: 17,)
                ],
              ),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left:8.0,right: 8),
                    child: Text("Hum: ${forecasList[index].humidity.toStringAsFixed(0)} %")
                  ),
                  //Icon(FontAwesomeIcons.solidGrinBeamSweat, color: Colors.white,size: 17,)
                ],
              ),
              Row(
                children: [
                  Padding(
                      padding: const EdgeInsets.only(left:8.0,right: 8),
                      child: Text("Win: ${forecasList[index].speed.toStringAsFixed(1)} km/h")
                  ),
                  //Icon(FontAwesomeIcons.solidGrinBeamSweat, color: Colors.white,size: 17,)
                ],
              ),
            ],
          )
        ],
      ),

    ],
  );
}