import 'package:intl/intl.dart';

class Util{
  static String appId="XXXXXXXXXXXXXXXXXX";

  static String getFormattedDate(DateTime dateTime){
    //utilisation du package intl
    return new DateFormat("EEEE d MMM y").format(dateTime);
  }
}